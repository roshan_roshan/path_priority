from .cfg.ControlFlow import get_cfg, gen_cfg, unhack
import ast
import astor
from graphviz import Digraph
import inspect
import matplotlib.pyplot as plt
import networkx as nx
import sys


def compute_dominator(cfg, start=0, key='parents'):
    # dominator tree of cfg compute with this function
    dominator = {}
    dominator[start] = {start}
    all_nodes = set(cfg.keys())
    rem_nodes = all_nodes - {start}
    for n in rem_nodes:
        dominator[n] = all_nodes
    c = True
    while c:
        c = False
        for n in rem_nodes:
            pred_n = cfg[n][key]
            doms = [dominator[p] for p in pred_n]
            i = set.intersection(*doms) if doms else set()
            v = {n} | i
            if dominator[n] != v:
                c = True
            dominator[n] = v
    return dominator


def compute_flow(pythonfile):
    # this function compute flow of program with using of dominator tree of cfg
    cfg, first, last = get_cfg(pythonfile)
    return cfg, compute_dominator(cfg, start=first), compute_dominator(
        cfg, start=last, key='children')


def to_graph(cache, arcs=[]):
    # convert CFG from CFGNode object in ControlFlow.py file format to graph with graphviz library
    id_lineno = dict()
    graph = Digraph(comment='Control Flow Graph')
    colors = {0: 'blue', 1: 'red'}
    kind = {0: 'T', 1: 'F'}
    cov_lines = set(i for i, j in arcs)
    for _, cnode in cache.items():
        lineno = cnode.lineno()
        shape, peripheries = 'oval', '1'
        if isinstance(cnode.ast_node, ast.AnnAssign):
            if cnode.ast_node.target.id in {'_if', '_for', '_while'}:
                shape = 'diamond'
            elif cnode.ast_node.target.id in {'enter', 'exit'}:
                shape, peripheries = 'oval', '2'
        else:
            shape = 'rectangle'
        graph.node(cnode.i(), "%d: %s" % (lineno, unhack(cnode.source())),
                   shape=shape, peripheries=peripheries)
        id_lineno[cnode.i()] = lineno
        for pn in cnode.parents:
            plineno = pn.lineno()
            if hasattr(pn, 'calllink') and pn.calllink > 0 and not hasattr(
                    cnode, 'calleelink'):
                graph.edge(pn.i(), cnode.i(), style='dotted', weight=100)
                continue

            if arcs:
                if (plineno, lineno) in arcs:
                    graph.edge(pn.i(), cnode.i(), color='green')
                elif plineno == lineno and lineno in cov_lines:
                    graph.edge(pn.i(), cnode.i(), color='green')
                # child is exit and parent is covered
                elif hasattr(cnode, 'fn_exit_node') and plineno in cov_lines:
                    graph.edge(pn.i(), cnode.i(), color='green')
                # parent is exit and one of its parents is covered.
                elif hasattr(pn, 'fn_exit_node') and len(
                        set(n.lineno() for n in pn.parents) | cov_lines) > 0:
                    graph.edge(pn.i(), cnode.i(), color='green')
                # child is a callee (has calleelink) and one
                # of the parents is covered.
                elif plineno in cov_lines and hasattr(cnode, 'calleelink'):
                    graph.edge(pn.i(), cnode.i(), color='green')
                else:
                    graph.edge(pn.i(), cnode.i(), color='red')
            else:
                order = {c.i(): i for i, c in enumerate(pn.children)}
                if len(order) < 2:
                    graph.edge(pn.i(), cnode.i())
                else:
                    o = order[cnode.i()]
                    graph.edge(pn.i(), cnode.i(),
                               color=colors[o], label=kind[o])
    return graph, id_lineno

def graph(cache):
    # show CFG networkx as plot
    g = nx.DiGraph()
    for _, cnode in cache.items():
        g.add_node(cnode.lineno())
        for pn in cnode.parents:
            if cnode.lineno() == 1:
                g.add_edge(pn.lineno(), "end", color="red")
            else:
                g.add_edge(pn.lineno(), cnode.lineno(), color="red")
    return g

def show(cache):
    # show CFG networkx as plot
    g = nx.DiGraph()
    for _, cnode in cache.items():
        g.add_node(cnode.lineno())
        for pn in cnode.parents:
            if cnode.lineno() == 1:
                g.add_edge(pn.lineno(), "end", color="red")
            else:
                g.add_edge(pn.lineno(), cnode.lineno(), color="red")

    edge_color_list = [g[e[0]][e[1]]['color'] for e in g.edges()]
    nx.draw(g, with_labels=True, edge_color=edge_color_list)
    plt.show()


def repair_dot(path, pairs):
    # this function edit dot file according to line number of python function code
    pairs["2"] = 0
    file = path + "/Digraph.gv"
    with open(file, "r") as f:
        lines = f.readlines()

    with open(file, "w") as f:
        f.write("digraph {\n")
        for line in lines:
            if "->" in line:
                if line[1:3].isdigit() and line[7:9].isdigit():
                    if str(pairs[line[1:3]]) != str(pairs[line[7:9]]):
                        f.write("\t" + str(pairs[line[1:3]]) + " -> " + str(pairs[line[7:9]]) + line[9:])
                elif line[1:3].isdigit() and line[7:8].isdigit():
                    if str(pairs[line[1:3]]) != str(pairs[line[7:8]]):
                        f.write("\t" + str(pairs[line[1:3]]) + " -> " + str(pairs[line[7:8]]) + line[8:])
                elif line[1:2].isdigit() and line[6:8].isdigit():
                    if str(pairs[line[1:2]]) != str(pairs[line[6:8]]):
                        f.write("\t" + str(pairs[line[1:2]]) + " -> " + str(pairs[line[6:8]]) + line[8:])
                elif line[1:2].isdigit() and line[6:7].isdigit():
                    if str(pairs[line[1:2]]) != str(pairs[line[6:7]]):
                        f.write("\t" + str(pairs[line[1:2]]) + " -> " + str(pairs[line[6:7]]) + line[7:])
                else:
                    pass
            else:
                if line[1:3].isdigit():
                    f.write("\t" + str(pairs[line[1:3]]) + line[3:])
                elif line[1:2].isdigit():
                    f.write("\t" + str(pairs[line[1:2]]) + line[2:])
        f.write("}")


def cyclomatic_complexity(cache):
    # compute cyclomatic complexity of CFG
    g = nx.DiGraph()
    for _, cnode in cache.items():
        g.add_node(cnode.lineno())
        for pn in cnode.parents:
            if cnode.lineno() == 1:
                g.add_edge(pn.lineno(), "end", color="red")
            else:
                g.add_edge(pn.lineno(), cnode.lineno(), color="red")
    e = len(g.edges)
    n = len(g.nodes)
    return e - n + 2


def extract_dot(func, path):
    # this function create and extrac dot file of Control Flow Graph
    graph, pairs = to_graph(gen_cfg(func))
    graph.save(directory=path)
    repair_dot(path, pairs)


def extract_pdf(func, path):
    # this function create and extrac pdf file and schema of Control Flow Graph
    graph, pairs = to_graph(gen_cfg(func))
    graph.render(directory=path)
    repair_dot(path, pairs)

def extract_graph(func):
    return graph(gen_cfg(func))

def show_graph(func):
    # show CFG networkx as plot
    return show(gen_cfg(func))


def max_line(input_path, file, func):
    # This function use for compute first and last line of a function defined in a python file
    ffile = open(input_path + "/" + file + ".py", "r")
    lines = ffile.readlines()
    rang = len(lines)
    name = "def " + str(func)
    min_number = float("inf")
    for number, line in enumerate(lines):
        if name in line:
            min_number = number + 1
        elif ("def" in line and number > min_number - 1):
            max_number = number
            break
        elif number == rang - 1:
            max_number = number + 1
        else:
            continue
    return max_number


def get_arguments(source):
    # This function extract argument of a function that given by user as SUT
    sourcee = ast.parse(source)
    for node in ast.walk(sourcee):
        if node.__class__.__name__ == "FunctionDef":
            for child in ast.iter_child_nodes(node):
                if hasattr(child, "args"):
                    return (astor.to_source(child))


def cyclomatic(func):
    # This function compute cyclomatic complexity of a SUT function cfg
    return cyclomatic_complexity(gen_cfg(func))

def first_line(input_path, file, func):
    # This function use for compute first and last line of a function defined in a python file
    ffile = open(input_path + "/" + file + ".py", "r")
    lines = ffile.readlines()
    name = "def " + str(func)
    min_number = float("inf")
    for number, line in enumerate(lines):
        if name in line:
            min_number = number
    return min_number
