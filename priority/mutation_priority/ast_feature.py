import inspect
import sys
import ast
import ast
from collections import defaultdict
import difflib
from astmonkey import visitors, transformers
import uuid
import re


class ast_feature:
    def __init__(self, file, function, input_dir, mutated_node, mutant_expression_from, mutant_expression_to, control_dependency):
        # get expressions nodes with parents and childrens and their types
        self.input_path = input_dir
        self.control_dependency = control_dependency
        self.file = file
        self.function = function
        self.mutated_node = mutated_node
        self.repair_expression(mutant_expression_from, mutant_expression_to)
        self.mutatn_expression = self.get_expression()
        # print(self.mutatn_expression)
        self.get_code()
        self.data_dependency, self.data_deps_line = self.get_deps()
        self.get_par_childe()
        self.get_variable_of_mutant()
        # print("data_dependency:          ", self.data_dependency)
        # print("data_dependency line:          ",  self.data_deps_line)

    def repair_expression(self, mutant_expression_from, mutant_expression_to):
        # delete if elif while expressions
        k = 5
        while mutant_expression_to[k] == ' ':
            k += 1
        self.mutant_expression_from = mutant_expression_from[k:]
        self.mutant_expression_to = mutant_expression_to[k:]
        self.block_type = "assign"
        if "if " in self.mutant_expression_to:
            self.block_type = "if"
            self.mutant_expression_to = self.mutant_expression_to.replace("if ", "")
            self.mutant_expression_to = self.mutant_expression_to[:-2]
            self.mutant_expression_from = self.mutant_expression_from.replace("if ", "")
            self.mutant_expression_from = self.mutant_expression_from[:-2]
        if "while " in self.mutant_expression_to:
            self.block_type = "while"
            self.mutant_expression_to = self.mutant_expression_to.replace("while ", "")
            self.mutant_expression_to = self.mutant_expression_to[:-2]
            self.mutant_expression_from = self.mutant_expression_from.replace("while ", "")
            self.mutant_expression_from = self.mutant_expression_from[:-2]
        if "for" in self.mutant_expression_to:
            for i in range(len(self.mutant_expression_from)):
                if self.mutant_expression_from[i] == "(":
                    num_open = i + 1
                if self.mutant_expression_from[i] == ")":
                    num_close = i
            for i in range(len(self.mutant_expression_to)):
                if self.mutant_expression_to[i] == "(":
                    num_open2 = i + 1
                if self.mutant_expression_to[i] == ")":
                    num_close2 = i
            fr = self.mutant_expression_from[num_open:num_close].split(",")
            tt = self.mutant_expression_to[num_open2:num_close2].split(",")
            # print(fr)
            # print(tt)
            if len(fr) == 2:
                if fr[0] == tt[0]:
                    self.mutant_expression_to = tt[1].replace(" ", "")
                    self.mutant_expression_from = fr[1].replace(" ", "")
                elif fr[1] == tt[1]:
                    self.mutant_expression_to = tt[0].replace(" ", "")
                    self.mutant_expression_from = fr[0].replace(" ", "")
            elif len(fr) == 3:
                if fr[0] == tt[0]:
                    self.mutant_expression_to = tt[1].replace(" ", "")
                    self.mutant_expression_from = fr[1].replace(" ", "")
                elif fr[1] == tt[1]:
                    self.mutant_expression_to = tt[0].replace(" ", "")
                    self.mutant_expression_from = fr[0].replace(" ", "")
                else:
                    self.mutant_expression_to = tt[2].replace(" ", "")
                    self.mutant_expression_from = fr[2].replace(" ", "")
        # print(self.mutant_expression_to)
        return self

    def get_expression(self):
        deletion = 0
        mut_expression = ""
        for i,s in enumerate(difflib.ndiff(self.mutant_expression_from, self.mutant_expression_to)):
            if s[0]==' ': continue
            elif s[0]=='-':
                deletion += 1
            elif s[0]=='+':
                mut_expression += s[-1]
                k = i
        if deletion == 0:
            inx = self.mutant_expression_to.find(mut_expression)
            if self.mutant_expression_to[inx - 1] == " ":
                mut_expression = mut_expression + self.mutant_expression_to[inx+1]
            elif self.mutant_expression_to[inx + 1] == " ":
                mut_expression = self.mutant_expression_to[k-1] + mut_expression
        if mut_expression == "!":
            mut_expression = "!="
        return mut_expression

    def get_code(self):
        # read program under test source code as input to adopted source code style
        sys.path.append(self.input_path)
        exec(f"from {self.file} import {self.function}")
        self.source = inspect.getsource(eval(f"{self.function}"))
        self.data_source = ""
        self.line = list()
        no = 0
        for i in self.source.splitlines():
            no += 1
            if " = " in i:
                self.line.append(no)
                k = 0
                while i[k] == ' ':
                    k += 1
                self.data_source += i[k:]
                self.data_source += "\n"
        return self

    def get_deps(self):
        deps = defaultdict(list)
        data_deps = defaultdict(list)
        data_deps_line = defaultdict(list)
        body = ast.parse(self.data_source)
        _, statements = next(ast.iter_fields(body))
        for assign in statements:
            for d in ast.walk(assign):
                if isinstance(d, ast.Name):
                    deps[assign.targets[0].id].append(d.id)
        for var in deps:
            for d in deps[var]:
                if d in deps and d != var:
                    data_deps[var].append(d)
        for deff, usee in data_deps.items():
            line = 0
            for i in str(self.data_source).splitlines():
                line += 1
                if deff in i:
                    def_key = self.line[line-1]
                    break
            line = 0
            for i in str(self.data_source).splitlines():
                line += 1
                for j in usee:
                    if deff in i and j in i and (i.find(deff) > i.find(j)):
                        # print(deff, j)
                        def_value =  self.line[line-1]
                        data_deps_line[def_key].append(def_value)
        # print(data_deps)
        # print(data_deps_line)
        return data_deps, data_deps_line

    def get_par_childe(self):
        # print("from:          ", self.mutant_expression_from)
        # print("To:            ", self.mutant_expression_to)
        tree = ast.parse(self.mutant_expression_to)
        node = transformers.ParentChildNodeTransformer().visit(tree)
        visitor = visitors.GraphNodeVisitor()
        visitor.visit(node)
        # print(type(visitor.graph))
        # visitor.graph.write_png(f'./pics/{str(uuid.uuid4())}.png')
        self.node_parent = node_parent_dict(visitor.graph)
        self.node_parent, self.parents, self.childs = mutant_parrent_child(self.node_parent, self.mutatn_expression)
        # print("parents:                  ", self.parents)
        # print("childs:                   ", self.childs)
        return self

    def get_variable_of_mutant(self):
        self.varss = list()
        for i, j in self.node_parent.items():
            if "id" in i:
                r1 = re.findall(r"['](.*)[']", i)
                for kk in r1:
                    self.varss.append(kk)
            if "id" in j:
                r1 = re.findall(r"['](.*)[']", i)
                for kk in r1:
                    self.varss.append(kk)
        # print(self.varss)
        return self

    def feature_extraction(self):
        ast_feature = {"AstNUmParents": self.AstNumParents(),
                               "NumOutDataDeps": self.NumOutDataDeps(),
                               "NumInDataDeps": self.NumInDataDeps(),
                               "NumOutCtrlDeps": self.NumOutCtrlDeps(),
                               "NumInCtrlDeps": self.NumInCtrlDeps(),
                               "TypeAstParent": self.TypeAstParent(),
                               "TypeMutant": self.TypeMutant(),
                               "TypeStmtBB": self.TypeStmtBB(),
                               "AstChildHasIdentifier": self.AstChildHasIdentifier(),
                               "AstChildHasLiteral": self.AstChildHasLiteral(),
                               "AstChildHasOperator": self.AstChildHasOperator()
                               }
        return ast_feature, self.data_deps_line, self.parents, self.mutatn_expression

    def AstNumParents(self):
        # Number of AST parents of EM
        # if type of block is while or if then parent number +1
        # else recursively count parent
        return len(self.parents)

    def NumOutDataDeps(self):
        # Number of mutants on expressions data-dependents on EM
        ans = list()
        for data, dependent in self.data_dependency.items():
            for var in self.varss:
                if var in dependent:
                    ans.append(data)
        # print("Out data dependency:      ", ans)
        return len(ans)

    def NumInDataDeps(self):
        # Number of mutants on expressions on which EM is data-dependent
        # lines which mutated line data depended to them
        ans = list()
        for data, dependent in self.data_dependency.items():
            for var in self.varss:
                if var == data:
                    for kk in dependent:
                        ans.append(kk)
        # print("In data dependency:      ", ans)
        return len(ans)

    def NumOutCtrlDeps(self):
        # Number of mutants on statements control-dependents on EM
        count = 0
        for _, deps in self.control_dependency.items():
            for k in deps:
                if str(k) == str(self.mutated_node):
                    count += 1
        return count

    def NumInCtrlDeps(self):
        # feature value is the number of mutants on expressions on which the mutated statement is control dependent.
        count = 0
        for node, deps in self.control_dependency.items():
            if str(self.mutated_node) == str(node):
                count = len(deps)
        return count

    def TypeAstParent(self):
        # Expression type of AST parent expressions of EM
        return self.parents

    def TypeMutant(self):
        # Mutant type of M as matched code pattern and replacement. Ex: a + b → a − b
        return self.mutant_expression_from + "-->" + self.mutant_expression_to

    def TypeStmtBB(self):
        # CFG basic block type of BM . Ex: if − then, if − else
        return self.block_type

    def AstChildHasIdentifier(self):
        # AST child of expression EM has an identifier
        ans = True
        for i in self.childs:
            if "id" not in i:
                ans = False
        return ans

    def AstChildHasLiteral(self):
        # AST child of expression EM has a literal
        ans = False
        for i in self.childs:
            if ("ast.Name" in i) or ("ast.Constant" in i):
                ans = True
        return ans

    def AstChildHasOperator(self):
        # AST child of expression EM has an operator
        ans = False
        for i in self.childs:
            if "ast.Nmae" not in i:
                ans = True
        return ans

    def DataTypeOfValue(self):
        # Data type of the returned value of EM
        if self.block_type in ["if", "while"]:
            ans = "Boolean"
        elif self.block_type in ["assign", "for"]:
            ans = "int/float"
        return ans

def node_parent_dict(dot):
    raw = dot.to_string()
    lines = raw.splitlines()
    nodes = list()
    edges = list()
    node_parent = dict()
    node_label = dict()
    # seprate nodes and edges
    for line in lines:
        if "--" in line:
            edges.append(line)
        else:
            nodes.append(line)
    nodes = nodes[2:-1]
    edges = edges[1:]
    # create node_label dictiorary
    for i in nodes:
        lb = list()
        count = 0
        for j in i:
            if j == "<":
                start = count
            elif j == ">":
                end = count
            elif j == '"':
                lb.append(count)
            count += 1
        node_label[i[start:end+1]] = i[lb[0]+1:lb[1]]
    # create node_parent dictionary
    for j in edges:
        count = 0
        baz = list()
        baste = list()
        for k in j:
            if k == "<":
                baz.append(count)
            elif k == ">":
                baste.append(count)
            count += 1
        node_parent[j[baz[1]:baste[1]+1]] = j[baz[0]:baste[0]+1]
    result = {}
    for node, parent in node_parent.items():
        result[node_label[node]] = node_label[parent]
    return result

def mutant_parrent_child(node_parent, mutant_expression):
    ans_par = list()
    ans_child = list()
    symbols = {
        "ast.And()": 'and',
        "ast.Or()": 'or',
        "ast.Add()": '+',
        "ast.Sub()": '-',
        "ast.Mult()": '*',
        "ast.Div()": '/',
        "ast.FloorDiv()": '//',
        "ast.Mod()": '%',
        "ast.LShift()": '<<',
        "ast.RShift()": '>>',
        "ast.BitOr()": '|',
        "ast.BitAnd()": '&',
        "ast.BitXor()": '^',
        "ast.Pow()": '**',
        "ast.Eq()": '==',
        "ast.Gt()": '>',
        "ast.GtE()": '>=',
        "ast.In()": 'in',
        "ast.Is()": 'is',
        "ast.IsNot()": 'is not',
        "ast.Lt()": '<',
        "ast.LtE()": '<=',
        "ast.NotEq()": '!=',
        "ast.NotIn()": 'not in',
        "ast.Invert()": '~',
        "ast.Not()": 'not ()',
        "ast.No ()": "not ",
        "ast.Expr()": 'root'}
    symbols = dict((v,k) for k,v in symbols.items())
    symbols['not '] = "ast.Not ()"
    answer = ""
    for i in symbols.keys():
        if mutant_expression == i:
            answer = symbols[i]

    new_node_parent = dict()
    for i, j in node_parent.items():
        if i == "ast.Compare()":
            new_node_parent[answer] = j
        elif j == "ast.Compare()":
            new_node_parent[i] = answer
        else:
            new_node_parent[i] = j
    # print(answer)
    # print("expression_parent_node:   ", new_node_parent)

    for i in new_node_parent.keys():
        if answer in i:
            ans_par.append(new_node_parent[i])
    for i, j in new_node_parent.items():
        if answer in j:
            ans_child.append(i)
    return new_node_parent, ans_par, ans_child
