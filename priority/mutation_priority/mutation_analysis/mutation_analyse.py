import json
import csv
import sys
import os


class mutation_function:
    def __init__(self, input_path, module_name, function_name, output_path, csv_file, reaport):
        self.input_path = input_path
        self.modul_name = module_name[:-3]
        self.function_name = function_name
        self.output_path = output_path
        self.csv = csv_file
        self.report = reaport
        self.data = list()
        self.result = list()
        self.creat_config()
        self.read_data_csv()
        self.create_result_with_oracle()

    def progress_report(self, count, total, status=''):
        bar_len = 50
        filled_len = int(round(bar_len * count / float(total)))
        percents = round(100.0 * count / float(total), 1)
        bar = '|' * filled_len + '_' * (bar_len - filled_len)
        sys.stdout.write('\r%s %s%s %s' % (bar, percents, '%', status))
        sys.stdout.flush()

    def creat_config(self):
        data = dict()
        data["config"] = list()
        data["config"].append({ "input_path": self.input_path,
                                               "module_name": self.modul_name,
                                               "function_name": self.function_name})
        with open("./priority/mutation_priority/mutation_analysis/config.json", "w") as file:
            json.dump(data, file)

    def read_data_csv(self):
        nums = list()
        # read data from csv and save in self.data
        csv_input = self.input_path + "/" + self.csv
        with open(csv_input) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',', lineterminator='\n')
            headers = next(csv_reader, None)
            for num, i in enumerate(headers):
                if "arg" in i:
                    nums.append(num)
            for row in csv_reader:
                args = "("
                for j in nums:
                    args += str(row[j]) + ","
                args = args[:-1] + ")"
                self.data.append(args)
            self.data = self.data[:int(len(self.data))]
        return self

    def create_result_with_oracle(self):
        sys.path.append(self.input_path)
        exec(f"from {self.modul_name} import {self.function_name}")
        # self.read_data_csv()
        for i in self.data:
            res = eval(f"{self.function_name}{i}")
            self.result.append(res)
        return self

    def write_to_json(self, innput, result):
        data = dict()
        data["data"] = list()
        data["data"].append({"input": innput,
                                            "result": result})
        with open("./priority/mutation_priority/mutation_analysis/data.json", "w") as file:
            json.dump(data, file)

    def delete_json(self):
        os.remove("./priority/mutation_priority/mutation_analysis/config.json")
        os.remove("./priority/mutation_priority/mutation_analysis/data.json")

    def create_directory(self):
        directory = "Mutation_analysis"
        parent_dir = self.output_path
        path = os.path.join(parent_dir, directory)
        os.mkdir(path)

    def run(self):
        count = 0
        rep = ""
        if "y" in self.report:
            # add rep yaml report
            pass
        if "h" in self.report:
            # add rep html report
            pass
        if "m" in self.report:
            rep += " -m"
        try:
            self.create_directory()
        except:
            pass
        # print(len(self.data))
        for i in range(len(self.data)):
            count += 1
            self.progress_report(count, len(self.data))
            self.write_to_json(self.data[i], self.result[i])
            now = str(self.data[i])
            os.system(f"echo '{now}' >> {self.output_path}/Mutation_analysis/data.txt")
            os.system(f"cd ./priority/mutation_priority/mutation_analysis & python ./mut.py --target {self.modul_name} --unit-test tester {rep} --disable-operator COI > {self.output_path}/Mutation_analysis/mutation_analysis{i}.txt")
        self.delete_json()

