import os
import csv
from collections import Counter
from collections import defaultdict
import numpy as np
from .ex_mutation_expression import *
from .cfg_feature import cfg_feature
from .ast_feature import ast_feature
from .mutation_analysis import mutation_function


class mutation_priority_list:
    def __init__(self, file, function, input_dir, output_dir, test_data) :
        self.output_dir = output_dir
        self.test_data = test_data
        self.input_dir = input_dir
        self.file = file
        self.function = function

    def generate_mutatnt(self):
        print("this process need several minutes")
        mutants = mutation_function(self.input_dir, self.file, self.function, self.output_dir, self.test_data, 'm')
        mutants.run()
        # print("mutant version is ready. please extract script with create_mutated_script() for next usage without using thist method.")

    def read_data(self):
        test_data = []
        with open(f"{self.output_dir}/Mutation_analysis/data.txt", 'r') as f:
            lines = f.readlines()
            for i in lines:
                test_data.append(i[1:-2])
        return test_data

    def read_mutation(self):
        mutated_to = list()
        mutated_from = list()
        lis = os.listdir(f"{self.output_dir}/Mutation_analysis")
        lis.remove( 'data.txt')
        with open(f"{self.output_dir}/Mutation_analysis/{lis[0]}") as f:
            lines = f.readlines()
            for i in lines:
                if "-" in i[0] and " " in i[1]:
                    mutated_from.append(i)
            for i in lines:
                if "+" in i[0] and " " in i[1]:
                    mutated_to.append(i)
        self.to = mutated_to
        self.fromm = mutated_from
        return self

    def create_mutated_script(self):
        self.goal_lines = list()
        directory = f"{self.output_dir}/mutation_codes"
        if not os.path.exists(directory):
            os.makedirs(directory)
        with open(f"{self.input_dir}/{self.file}", 'r') as source:
            lines = source.readlines()
        count = 0
        for i in self.to:
            count += 1
            ix = i.find(":")
            # print(i[ix-2:ix])
            goal_line = int(i[ix-2:ix])
            self.goal_lines.append(goal_line)
            with open(f"{self.input_dir}/{self.file}", "r") as inf:
                space = inf.readlines()[goal_line-1]
                for num, ka in enumerate(space):
                    if ka != " ":
                        sp = " " * num
                        break
            for numm, kk in enumerate(i[6:]):
                if kk != " ":
                    spp = numm
                    break
            with open(f"{self.output_dir}/mutation_codes/mutation{count}.py", 'w') as f:
                for j in range(0, goal_line - 1):
                    f.write(lines[j])
                f.write(sp + i[6+spp:])
                for k in range(goal_line, len(lines)):
                    f.write(lines[k])
        print("script of mutation versions extracted to output directory")
        return self.goal_lines

    def kill_survive(self):
        test_data = self.read_data()
        test_data_mutant = list()
        mutat = os.listdir(f"{self.output_dir}/Mutation_analysis")
        mutat.remove( 'data.txt')
        for i in range(0, len(mutat)):
            k = [test_data[i]]
            with open(f"{self.output_dir}/Mutation_analysis/mutation_analysis{i}.txt", 'r') as f:
                lines = f.readlines()
                for i in lines[0:-5]:
                    if "killed" in i:
                        k.append("killed")
                    elif "survived" in i:
                        k.append("survived")
                    elif "timeout" in i:
                        k.append("timeout")
            test_data_mutant.append(k)
        return test_data_mutant

    def killablity(self, rows):
        kill = [0] * (len(rows[0]))
        for i in range(0, len(rows)):
            for j in range(0, len(rows[0])):
                if rows[i][j] == "killed":
                    kill[j] = kill[j] + 1
        for k in range(1, len(kill)):
            kill[k] = kill[k] / len(rows)
        return kill

    def extract_feature_to_csv(self):
        rows = self.kill_survive()
        self.killblity = self.killablity(rows)
        features = self.all_feature_list
        with open(f"{self.output_dir}/dataset.csv", "w") as f:
            head = list(self.all_feature_list[0].keys())
            head.append("killability")
            writer = csv.writer(f)
            writer.writerow(head)
            for num, i in enumerate(features):
                body = list(i.values())
                body.append(self.killblity[num])
                writer.writerow(body)

    def extract_test_data_result_to_csv(self):
        rows = self.kill_survive()
        self.killblity = self.killablity(rows)
        # print(len(rows[0]))
        with open(f'{self.output_dir}/test_data_mutant.csv', 'w') as f:
            head = [f"mutant{i}" for i in range(1, len(rows[0] ))]
            head.insert(0, "test_data")
            write = csv.writer(f)
            write.writerow(head)
            write.writerows(rows)
            write.writerow(self.killblity)
        print("result of mutation and test data combination extracted to output")
        return self

    def mutant_feature(self):
        self.all_feature_list = list()
        mutant_code = os.listdir(f"{self.output_dir}/mutation_codes")
        self.node_expression = self.extract_node_expresion()
        for i in range(0, len(mutant_code) - 1):
            print(f"\nmutation{i+1}: ")
            print("--------------------------------")
            ix = self.to[i].find(":")
            mutated_node = self.to[i][ix-2: ix]
            mutant_expression_from = self.fromm[i]
            mutant_expression_to = self.to[i]
            print(mutant_expression_from, mutant_expression_to)
            new = self.feature_extraction(f"mutation{i+1}", mutated_node, mutant_expression_from, mutant_expression_to)
            self.all_feature_list.append(new)
            # print(new)
        return self.all_feature_list

    def extract_node_expresion(self):
        node_expression = defaultdict(list)
        mutant_code = os.listdir(f"{self.output_dir}/mutation_codes")
        in_path = f"{self.output_dir}/mutation_codes"
        for i in range(0, len(mutant_code) - 1):
            ix = self.to[i].find(":")
            mutated_node = self.to[i][ix-2: ix]
            mutant_expression_from = self.fromm[i]
            mutant_expression_to = self.to[i]
            expression = expression_line(f"mutation{i+1}", self.function, in_path, mutated_node, mutant_expression_from, mutant_expression_to)
            exp = expression.get_expression()
            node_expression[mutated_node].append(exp)
        return node_expression

    def complexity_of_statement(self, mutate_node):
        node_list = list()
        for i in self.to:
            ix = i.find(":")
            node_list.append(i[ix-2:ix])
        counts = dict(Counter(node_list))
        return counts, dict({'statement_complexity': counts[mutate_node]})

    def feature_extraction(self, file_name, mutated_node, mutant_expression_from ,mutant_expression_to):
        all_feature = []
        try:
            in_path = f"{self.output_dir}/mutation_codes"
            counts, statement_complexity = self.complexity_of_statement(mutated_node)
            cfg_f = cfg_feature(file_name, self.function, in_path, mutated_node)
            cfg_featu = cfg_f.feature_extraction()
            cfg_featuree = dict()
            cfg_featuree.update(statement_complexity)
            cfg_featuree.update(cfg_featu)
            control_dependency = cfg_f.control_dependency()
            ast_f = ast_feature(file_name, self.function, in_path, mutated_node, mutant_expression_from, mutant_expression_to, control_dependency)
            ast_featuree, data_deps_line, parents, mutation_expression = ast_f.feature_extraction()
            # print("numu",mutated_node)
            # print(ast_featuree)
            ast_pfeature = ast_parent_features(parents, mutated_node, counts, data_deps_line, control_dependency, self.node_expression)
            ast_featuree.update(ast_pfeature)
            all_feature = cfg_featuree.copy()
            all_feature.update(ast_featuree)
        except:
            pass
        return all_feature

def ast_parent_features(parents, mutated_node, mutated_node_counts, data_deps_line, control_dependency, node_expression):
    # print("parents", parents)
    # print("mutated_node", mutated_node)
    # print("mutated_node_counts", mutated_node_counts)
    # print("node_expression", node_expression)
    # print("data_deps_line", data_deps_line)
    # print("control_dependency", control_dependency)

    # Mutant types of M’s AST parents
    AstParentMutantType = parents
    # Number of mutants on expressions data-dependent onEM ’s AST parent statement
    AstParentsNumOutDataDeps = 0
    # Number of mutants on expressions on which EM ’s AST parent expression is data-dependent
    AstParentsNumInDataDeps = 0
    # Mutant types of mutants on expressions data-dependents on EM
    OutDataDepMutantType = list()
    # Mutant types of mutants on expressions on which EM is data-dependent
    InDataDepMutantType = list()

    for node, dependent in data_deps_line.items():
        if int(mutated_node) in dependent:
            if str(node) in mutated_node_counts.keys():
                AstParentsNumOutDataDeps += mutated_node_counts[str(node)]
            if str(node) in node_expression.keys():
                OutDataDepMutantType += node_expression[str(node)]
        if str(node) == str(mutated_node):
            for kk in dependent:
                if str(kk) in mutated_node_counts.keys():
                    AstParentsNumInDataDeps += mutated_node_counts[str(kk)]
                if str(kk) in node_expression.keys():
                    InDataDepMutantType += node_expression[str(kk)]

    # Number of mutants on statements control-dependent on EM ’s AST parent expression
    AstParentsNumOutCtrlDeps = 0
    # Number of mutants on expressions on which EM ’s AST parent expression is control-dependent
    AstParentsNumInCtrlDeps = 0
    # Mutant types of mutants on statements control-dependents on EM
    InCtrlDepMutantType = list()
    # Mutant types of mutants on expressions on which EM is control-dependent
    OutCtrlDepMutantType = list()

    for node, dependent in control_dependency.items():
        if int(mutated_node) in dependent:
            if str(node) in mutated_node_counts.keys():
                AstParentsNumInCtrlDeps += mutated_node_counts[str(node)]
            if str(node) in node_expression.keys():
                OutCtrlDepMutantType += node_expression[str(node)]
        if str(node) == str(mutated_node):
            for kk in dependent:
                if str(kk) in mutated_node_counts.keys():
                    AstParentsNumOutCtrlDeps += mutated_node_counts[str(kk)]
                if str(kk) in node_expression.keys():
                    InCtrlDepMutantType += node_expression[str(kk)]

    ast_pfeature = {"AstParentMutantType": AstParentMutantType,
                             "AstParentsNumOutDataDeps": AstParentsNumOutDataDeps,
                             "AstParentsNumInDataDeps": AstParentsNumInDataDeps,
                             "AstParentsNumOutCtrlDeps": AstParentsNumOutCtrlDeps,
                             "AstParentsNumInCtrlDeps": AstParentsNumInCtrlDeps,
                             "OutDataDepMutantType": aggrigation_score(OutDataDepMutantType),
                             "InDataDepMutantType": aggrigation_score(InDataDepMutantType),
                             "OutCtrlDepMutantType": aggrigation_score(OutCtrlDepMutantType),
                             "InCtrlDepMutantType": aggrigation_score(InCtrlDepMutantType),
                             }
    return ast_pfeature

def aggrigation_score(list_of_expressions):
    scores = ['', '+', '-', '*', '/', '//', '%' , 'and', 'or', '<<', '>>', '|', '&',  '^', '**', '==', '>', '>=', 'in', 'is', 'is not', '<',  '<=', '!=', "=", 'not in', '~', 'not ()', "not "]
    result = np.array([0.]*len(scores))
    scores_arr = np.eye(len(scores))
    for i in list_of_expressions:
        if i == "* ":
            i = "*"
        if i == "continu":
            i = ''
        result += scores_arr[scores.index(i)]
    return result