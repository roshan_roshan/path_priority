import inspect
import sys
import ast
import ast
from collections import defaultdict
import difflib
from astmonkey import visitors, transformers
import uuid
import re


class expression_line:
    def __init__(self, file, function, input_dir, mutated_node, mutant_expression_from, mutant_expression_to):
        # get expressions nodes with parents and childrens and their types
        self.input_path = input_dir
        self.file = file
        self.function = function
        self.mutated_node = mutated_node
        self.repair_expression(mutant_expression_from, mutant_expression_to)

    def repair_expression(self, mutant_expression_from, mutant_expression_to):
        # delete if elif while expressions
        k = 5
        while mutant_expression_to[k] == ' ':
            k += 1
        self.mutant_expression_from = mutant_expression_from[k:]
        self.mutant_expression_to = mutant_expression_to[k:]
        self.block_type = "assign"
        if "if " in self.mutant_expression_to:
            self.block_type = "if"
            self.mutant_expression_to = self.mutant_expression_to.replace("if ", "")
            self.mutant_expression_to = self.mutant_expression_to[:-2]
            self.mutant_expression_from = self.mutant_expression_from.replace("if ", "")
            self.mutant_expression_from = self.mutant_expression_from[:-2]
        if "while " in self.mutant_expression_to:
            self.block_type = "while"
            self.mutant_expression_to = self.mutant_expression_to.replace("while ", "")
            self.mutant_expression_to = self.mutant_expression_to[:-2]
            self.mutant_expression_from = self.mutant_expression_from.replace("while ", "")
            self.mutant_expression_from = self.mutant_expression_from[:-2]
        return self

    def get_expression(self):
        deletion = 0
        mut_expression = ""
        for i,s in enumerate(difflib.ndiff(self.mutant_expression_from, self.mutant_expression_to)):
            if s[0]==' ': continue
            elif s[0]=='-':
                deletion += 1
            elif s[0]=='+':
                mut_expression += s[-1]
                k = i
        if deletion == 0:
            inx = self.mutant_expression_to.find(mut_expression)
            if self.mutant_expression_to[inx - 1] == " ":
                mut_expression = mut_expression + self.mutant_expression_to[inx+1]
            elif self.mutant_expression_to[inx + 1] == " ":
                mut_expression = self.mutant_expression_to[k-1] + mut_expression
        if mut_expression == "!":
            mut_expression = "!="
        return mut_expression
