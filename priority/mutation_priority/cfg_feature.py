import sys
import inspect
from functools import reduce
from collections import defaultdict
from ..test_tools import extract_graph
import networkx as nx
import matplotlib.pyplot as plt


class cfg_feature:
    def __init__(self, file, function, input_path, mutated_node):
        # mutated_node as number
        self.input_path = input_path
        self.file = file
        self.function = function
        self.m_node = mutated_node
        self.get_code()
        self.graph()

    def get_code(self):
        # read program under test source code as input to adopted source code style
        sys.path.append(self.input_path)
        exec(f"from {self.file} import {self.function}")
        self.source = inspect.getsource(eval(f"{self.function}"))
        return self

    def graph(self):
        # ecxtract networkx and plot format of control flow graph of program under test
        self.graph = extract_graph(self.source)
        return self

    def  CfgDepth(self):
        # Depth of BM according to CFG
        distance = 0
        try:
            distance = len(nx.shortest_path(self.graph, source=1, target=int(self.m_node)))
        except:
            pass
        return distance

    def CfgPredNum(self):
        # Number of predecessor basic blocks, according to CFG, of BM
        num_pred = 0
        try:
            pred = list(nx.DiGraph.predecessors(self.graph, int(self.m_node)))
            num_pred = len(pred)
        except:
            pass
        return num_pred

    def CfgSuccNum(self):
        # Number of successors basic blocks, according to CFG, of BM
        num_succ = 0
        try:
            succ = list(nx.DiGraph.successors(self.graph, int(self.m_node)))
            num_succ = len(succ)
        except:
            pass
        return num_succ

    def feature_extraction(self):
        res_dict = {'CfgDepth': self.CfgDepth(), 'CfgPredNum': self.CfgPredNum(), 'CfgSuccNum':self.CfgSuccNum()}
        return res_dict

    def post_dominant(self):
        post_dominant = defaultdict(list)
        # postdomination_check(self.graph, x, y)
        node = list(self.graph.nodes())
        node.remove(1)
        for i in range(len(node)):
            paths = []
            for path in nx.all_simple_paths(self.graph, node[i], "end"):
                paths.append(path)
            try:
                intersect = list(reduce(set.intersection, [set(item) for item in paths]))
                for k in intersect:
                    if k != node[i]:
                        post_dominant[k].append(node[i])
            except:
                pass
        del post_dominant["end"]
        # print(post_dominant)
        # nx.draw(self.graph, with_labels=True)
        # plt.show()
        return post_dominant

    def control_dependency(self):
        cdp = defaultdict(list)
        dict_post_dom = self.post_dominant()
        node = list(self.graph.nodes())
        node.remove(1)
        node.remove('end')
        for i in range(len(node)):
             for j in range(len(node)):
                 if node[i] != node[j]:
                     for path in nx.all_simple_paths(self.graph, node[i], node[j]):
                         now_path = path.copy()
                         now_path.remove(node[i])
                         now_path.remove(node[j])
                         # print(node[i], node[j],":--------")
                         # print(now_path)
                         # print("post_dom", dict_post_dom[node[j]])
                         if list(dict_post_dom[node[j]]) != [] :
                             if all(i in dict_post_dom[node[j]]  for i in now_path) and node[i] not in dict_post_dom[node[j]]:
                                 if node[i] not in cdp[node[j]]:
                                    cdp[node[j]].append(node[i])
        return cdp


def sublist(lst1, lst2):
   ls1 = [element for element in lst1 if element in lst2]
   ls2 = [element for element in lst2 if element in lst1]
   return ls1 == ls2
