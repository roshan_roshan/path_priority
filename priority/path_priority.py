import sys, inspect, os, csv, difflib
import numpy as np
import random
import joblib
from collections import defaultdict, OrderedDict
import sklearn
from .test_tools import extract_graph, extract_dot, get_arguments, first_line
from priority import mutation_priority_list
from .code_coverage import cfg_coverage


class path_prioritize:
    def __init__(self, file, function, input_dir, output_dir):
        self.file = file
        self.function = function
        self.input_dir = input_dir
        self.output_dir = output_dir
        self._read_datas()

    def _get_code(self):
        # read program under test source code as input to adopted source code style
        sys.path.append(self.input_dir)
        exec(f"from {self.file[:-3]} import {self.function}")
        self.source = inspect.getsource(eval(f"{self.function}"))
        return self

    def _graph(self):
        # ecxtract networkx and plot format of control flow graph of program under test
        self.graph = extract_graph(self.source)
        return self

    def _extract_dot(self):
        self._get_code()
        self._graph()
        path = self.output_dir
        extract_dot(self.source, path)

    def _read_datas(self):
        path = os.path.dirname(os.path.abspath(__file__))
        self.type_parent = joblib.load(f"{path}/datas/type_parent.pkl")
        self.type_dict = joblib.load(f"{path}/datas/types_dict.pkl")
        self.type_parents_dict = joblib.load(f"{path}/datas/types_parent_dict.pkl")
        self.types_mutant = joblib.load(f"{path}/datas/types_mutant_dict.pkl")
        self.type_stmt = joblib.load(f"{path}/datas/type_stm.pkl")
        self.model = joblib.load(f"{path}/datas/DT_model.pkl")
        return self

    def _create_csv(self):
        values = list()
        str_args = get_arguments(self.source)
        arguments = str_args.split(",")
        len_argument = len(arguments)
        for i in arguments:
            type_arg =i.split(":")[1]
            if type_arg == " int":
                num = random.randint(0, 100)
            if type_arg == " float":
                num = random.uniform(0, 100)
            values.append(num)
        head = [f"arg{i}" for i in range(len_argument)]
        with open(f"{self.input_dir}/fake_input.csv", "w") as f:
            writer = csv.writer(f, delimiter=',', lineterminator='\n')
            writer.writerow(head)
            writer.writerow(values)
        return "fake_input.csv"

    def _feature_extraction(self):
        # input generation for type and create csv file
        file = self._create_csv()
        feature_of_mutant = mutation_priority_list(self.file, self.function, self.input_dir, self.output_dir, file)
        feature_of_mutant.generate_mutatnt()
        feature_of_mutant.read_mutation()
        self.goal_lines = feature_of_mutant.create_mutated_script()
        first = first_line(self.input_dir, self.file[:-3], self.function)
        for i in range(len(self.goal_lines)):
            self.goal_lines[i] = self.goal_lines[i] - first

        self.features = feature_of_mutant.mutant_feature()
        return self

    def _repair_features(self):
        self.features = list(filter(None, self.features))
        for mutant in self.features:
            del mutant["AstParentMutantType"]
            mid = ""
            for j in mutant["TypeAstParent"]:
                if "ast.Name" not in j:
                    mid += str(self.type_parent[j]) + ","
                else:
                    mid += str(self.type_parent["ast.Name"]) + ","
            try:
                mutant["TypeAstParent"] = self.type_parents_dict[mid[0:-1]]
            except KeyError:
                mutant["TypeAstParent"] = 100000

            mutant["TypeStmtBB"] = self.type_stmt[mutant["TypeStmtBB"]]

            two = mutant["TypeMutant"].split("-->")
            case_a = two[0]
            case_b = two[1]
            output_list = [li for li in difflib.ndiff(case_a, case_b) if li[0] != ' ']
            minus = ""
            plus = ""
            for k in output_list:
                if k[0] == "-":
                    minus += k[2]
                if k[0] == "+":
                    plus += k[2]
            change = minus + "-->" + plus
            mutant["TypeMutant"] =self.types_mutant[change]
            ans = 0
            for num, j in enumerate(mutant["OutDataDepMutantType"]):
                ans += (num+1) * int(j)
            mutant["OutDataDepMutantType"] = ans
            ans = 0
            for num, j in enumerate(mutant["InDataDepMutantType"]):
                ans += (num+1) * int(j)
            mutant["InDataDepMutantType"] = ans
            ans = 0
            for num, j in enumerate(mutant["OutCtrlDepMutantType"]):
                ans += (num+1) * int(j)
            mutant["OutCtrlDepMutantType"] = ans
            ans = 0
            for num, j in enumerate(mutant["InCtrlDepMutantType"]):
                ans += (num+1) * int(j)
            mutant["InCtrlDepMutantType"] = ans

    def _mutation_prioritize(self):
        self.goal_score = defaultdict(list)
        self._feature_extraction()
        self._repair_features()
        for num, i in enumerate(self.features):
            row = [ int(x) for x in i.values() ]
            row = np.array(row).reshape(1, -1)
            self.goal_score[self.goal_lines[num]].append(1 - (self.model.predict(row)[0] / 10))
        return self.goal_score

    def _path_extraction(self):
        # MCTP extraction
        self._extract_dot()
        gv_path = self.output_dir + "/Digraph.gv"
        self.cfg_dot = cfg_coverage(gv_path, 1, 0)
        self.simple_pathes = self.cfg_dot.simple_path()
        self.prime_pathes = self.cfg_dot.prime_path()
        self.prime_coverage, self.prime_req = self.cfg_dot.prime_path_coverage_bruteforce()
        self.prime_coverage1, self.prime_req1 = self.cfg_dot.prime_path_coverage_setcoverage()
        return self

    def path_priority(self):
        path_score = dict()
        self._path_extraction()
        self._mutation_prioritize()
        mut_score = dict()
        for node, score in self.goal_score.items():
            ave_score = 0
            for k in score:
                ave_score += k
            ave_score = ave_score / len(score)
            mut_score[node] = ave_score
        # compute nodes fault potential for each path
        for path in self.prime_coverage:
            p_score = 0
            for i in path:
                if i in mut_score.keys():
                   p_score += mut_score[i]
            path_score[str(path)] = p_score
        oredered_fault_prone_path = OrderedDict(sorted(path_score.items(), reverse=True, key=lambda t: t[1]))
        return oredered_fault_prone_path
