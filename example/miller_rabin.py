import random


def miller_rabin(n: int, prec: int):
    if n < 2:
        return False
    if n % 2 == 0:
        return n == 2
    d = n - 1
    exp = 0
    while d % 2 == 0:
        d /= 2
        exp += 1
    count = 0
    while count < prec:
        a = random.randint(2, n - 1)
        b = bin_exp_mod(a, d, n)
        if b != 1:
            flag = True
            for i in range(exp):
                if b == n - 1:
                    flag = False
                    break
                b = b * b
                b %= n
            if flag:
                return False
            count += 1
    return True


def bin_exp_mod(a, n, b):
    if n == 0:
        return 1
    if n % 2 == 1:
        return (bin_exp_mod(a, n - 1, b) * a) % b
    r = bin_exp_mod(a, n / 2, b)
    return (r * r) % b
