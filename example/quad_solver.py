import math


def quad_solver(a: int, b: int, c: int) -> (str, tuple):
    if a == 0:
        return "error"
    discriminant = b**2 - 4 * a * c
    r1 = 0 
    r2 = 0
    i1 = 0
    i2 = 0
    if discriminant >= 0:
        droot = math.sqrt(discriminant)
        r1 = (-b + droot) / (2 * a)
        r2 = (-b - droot) / (2 * a)
    else:
        droot = math.sqrt(-1 * discriminant)
        droot_ = droot / (2 * a)
        r1 = -b / (2 * a)
        i1 = droot_
        r2 = -b / (2 * a)
        i2 = -droot_
    return r1
