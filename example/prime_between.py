def prime_between(lower:int, upper:int):
    lower = int(lower)
    upper = int(upper)
    for num in range(lower, upper + 1):
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                    break
        else:
            return num
