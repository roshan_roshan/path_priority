from decimal import Decimal


def Binominal(n: int, k: int) -> int:
    if k > n:
        return 0
    result = 1
    if k > n - k:
        k = n - k
    i = 1
    while i <= k:
        result *= n
        result //= i
        n -= 1
        i += 1
    return result


def pvalue(a: int, b: int, c: int, d: int) -> Decimal:
    return (Decimal(Binominal(a + b, a) * Binominal(c + d, c)) / Decimal(Binominal(a + b + c + d, a + c)))


def fisher(a: int, b: int, c: int, d: int) -> Decimal:
    if a == b and b == c and c == d:
        return str(Decimal(1))
    p = pvalue(a, b, c, d)
    t = p
    pp = 0.0
    curP = float(t)
    while(a > 0 and d > 0):
        curP *= a * d
        a -= 1
        b += 1
        c += 1
        d -= 1
        curP /= b * c
        if curP <= t:
            pp += curP
    leftTail = Decimal(pp)
    p += leftTail
    pp = float(0)
    curP = float(t)
    while(b > 0 and c > 0):
        curP *= b * c
        a += 1
        b -= 1
        c -= 1
        d += 1
        curP /= a * d
        if curP <= t:
            pp += curP
    rightTail = Decimal(pp)
    p += rightTail
    return str(p)
